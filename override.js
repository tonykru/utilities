function override(obj, fnName, fnBefore, fnAfter) {

	if (obj && obj[fnName]) {

		var fn;
		if (obj.__proto__[fnName]) {
			fn = obj.__proto__[fnName];
			obj.__proto__[fnName] = function () {
				fnBefore(arguments);
				var x = fn.apply(this, arguments);
				fnAfter(arguments, x);
				if (x !== undefined) {		
                    return x;
                }
            }
        } else {
			fn = obj[fnName];
			obj[fnName] = function () {
				fnBefore(arguments);
				var x = fn.apply(this, arguments);
				fnAfter(arguments, x);
				if (x !== undefined) {		
                    return x;
                }
            }
        }
    }
}
//test
override(window, 'Number', function () {console.log('bf')}, function () {console.log('at')});
console.log(Number(2))

override(new XMLHttpRequest(), 'open', function () {console.log('bf')}, function () {console.log('at')});